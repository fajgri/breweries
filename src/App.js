import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import StorieList from './components/StorieList';
import StorieListHeading from './components/StorieListHeading';
import SearchBox from './components/SearchBox';
import AddFavourites from './components/AddFavourites';
import RemoveFavourites from './components/RemoveFavourites';

const App = () => {
  const [breweries, setBreweries] = useState([]);
  const [favourites, setFavourites] = useState([]);
  const [searchValue, setSearchValue] = useState('');

  const getMovieRequest = async (searchValue) => {
    const url = `https://api.openbrewerydb.org/breweries/search?query=${searchValue}`;

    const response = await fetch(url);
    const responseJson = await response.json();

    if (responseJson) {
      setBreweries(responseJson);
    }
  };

  useEffect(() => {
    getMovieRequest(searchValue);
  }, [searchValue]);

  useEffect(() => {
    const movieFavourites = JSON.parse(
        localStorage.getItem('react-movie-app-favourites')
    );

    if (movieFavourites) {
      setFavourites(movieFavourites);
    }
  }, []);

  const saveToLocalStorage = (items) => {
    localStorage.setItem('react-movie-app-favourites', JSON.stringify(items));
  };

  const addFavouriteMovie = (brewerie) => {
    const newFavouriteList = [...favourites, brewerie];
    setFavourites(newFavouriteList);
    saveToLocalStorage(newFavouriteList);
  };

  const removeFavouriteMovie = (brewerie) => {
    const newFavouriteList = favourites.filter(
        (favourite) => favourite.id !== brewerie.id
    );

    setFavourites(newFavouriteList);
    saveToLocalStorage(newFavouriteList);
  };

  return (
      <div className='container-fluid movie-app'>
        <div className='row d-flex align-items-center mt-4 mb-4'>
          <StorieListHeading heading='breweries' />
          <SearchBox searchValue={searchValue} setSearchValue={setSearchValue} />
        </div>
        <div className='row'>
          <StorieList
              breweries={breweries}
              handleFavouritesClick={addFavouriteMovie}
              favouriteComponent={AddFavourites}
          />
        </div>
        <div className='row d-flex align-items-center mt-4 mb-4'>
          <StorieListHeading heading='Favourites' />
        </div>
        <div className='row'>
          <StorieList
              breweries={favourites}
              handleFavouritesClick={removeFavouriteMovie}
              favouriteComponent={RemoveFavourites}
          />
        </div>
      </div>
  );
};

export default App;
