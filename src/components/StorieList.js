import React from 'react';

const StorieList = (props) => {
    const FavouriteComponent = props.favouriteComponent;

    return (
        <>
            {props.breweries.map((brewerie, index) => (
                <div className='image-container d-flex justify-content-start m-4 '>

                    <table className="table " bgcolor= "#f0f8ff">
                        <thead>
                        <tr>
                            <th>Name:</th>
                            <th>{brewerie.name}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Brewery Type:</td>
                            <td>{brewerie.brewery_type}</td>
                        </tr>
                        <tr>
                            <td>Street:</td>
                            <td>{brewerie.street}</td>
                        </tr>
                        <tr>
                            <td>Address 2:</td>
                            <td>{brewerie.address_2}</td>
                        </tr>
                        <tr>
                            <td>Address 3:</td>
                            <td>{brewerie.address_3}</td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td>{brewerie.city}</td>
                        </tr>
                        <tr>
                            <td>State:</td>
                            <td>{brewerie.state}</td>
                        </tr>
                        <tr>
                            <td>County Province:</td>
                            <td>{brewerie.county_province}</td>
                        </tr>
                        <tr>
                            <td>Postal Code:</td>
                            <td>{brewerie.postal_code}</td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>{brewerie.country}</td>
                        </tr>
                        <tr>
                            <td>Longitude:</td>
                            <td>{brewerie.longitude}</td>
                        </tr>
                        <tr>
                            <td>Latitude:</td>
                            <td>{brewerie.latitude} </td>
                        </tr>
                        <tr>
                            <td>Phone:</td>
                            <td>{brewerie.phone}</td>
                        </tr>
                        <tr>
                            <td>Website Url:</td>
                            <td>{brewerie.website_url}</td>
                        </tr>
                        <tr>
                            <td>Updated At:</td>
                            <td>{brewerie.updated_at}</td>
                        </tr>
                        <tr>
                            <td>Created At:</td>
                            <td>{brewerie.created_at}</td>
                        </tr>
                        </tbody>
                    </table>


                    <div
                        onClick={() => props.handleFavouritesClick(brewerie)}
                        className='overlay d-flex align-items-center justify-content-center'
                    >
                        <FavouriteComponent />
                    </div>
                </div>
            ))}
        </>
    );
};

export default StorieList;
